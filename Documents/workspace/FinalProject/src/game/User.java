package game;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;

public class User {
	static final long serialVersionUID = 1;
	private String name;
	private int money;
	User(){
		
	}
	
	User(String n, int m){
		name = n;
		money = m;
	}
	
	public void init(String n, int m){
		name = n;
		money =m;
	}
	public String getUserName(){
		return this.name;
	}
	
	public int getUserMoney(){
		return this.money;
	}	

	
	public void setUserName(String n){
		this.name = n;
	}
	
	public void setUserMoney(int m){
		this.money = m;
	}


	}
	